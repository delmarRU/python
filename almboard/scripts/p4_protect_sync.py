﻿import pymongo
import P4
from datetime import datetime
from pymongo import MongoClient
from bson.objectid import ObjectId
from P4 import *


Mongo_SRV = 'almboard'
Mongo_DB = 'almboard'
Mongo_Coll = 'P4_users'
Mongo_Coll_AD = 'AD_users'
Mongo_Col_P4_Perm = 'P4_permissions'
Mongo_Col_Mail = 'Mail_Log'
Mongo_Col_TFSManagers = 'TFS_managers'


conn = MongoClient(Mongo_SRV, 27017)
db = conn[Mongo_DB]
coll = db[Mongo_Coll]
coll_adusers = db[Mongo_Coll_AD]
coll_p4_perm = db[Mongo_Col_P4_Perm]

type_accounts = {'user': 'User',
                 'service': 'Service account',
                 'shared': 'Shared account',
                 'user_no_delete': 'User (not delete)'}

# Текущая дата
def GetDate():
    d = str(datetime.datetime.now())[:-7]
    return d

################################################################################
### Perforce
################################################################################

def P4GetAllUsers():
    p4 = P4()  # Create the P4 instance
    p4.user = "TFS_Integr_Dev"
    p4.client = "SHAKIROV-O"  # Set some environment variables
    p4.connect()

    res = p4.run('users')

    p4.disconnect()  # Disconnect from the server

    return res


			
#### P4 Permissions ###################################
def P4GetProtect():
    p4 = P4()  # Create the P4 instance
    p4.user = "TFS_Integr_Dev"
    p4.client = "SHAKIROV-O"  # Set some environment variables
    p4.connect()

    spec=p4.run('protect', "-o")
    p4.disconnect()  # Disconnect from the server

    return spec

class P4Perm:
    def __init__(self, accesslevel, type, name, host, folder):
        self.AccessLevel = accesslevel
        self.Type = type
        self.Name = name
        self.Host = host
        self.Folder = folder
		
def P4Perm_Parsing(protect):
    permissions = protect[0]['Protections']
    perms = []

    for p in permissions:
        t = re.findall(r'"[^"]*"|\S+', p)
        perm = P4Perm(accesslevel=t[0], type=t[1], name=t[2], host=t[3], folder=t[4])
        perms.append(perm)

    return perms

# Полная выгрузка Protect в Mongo С ПЕРЕЗАПИСЬЮ!
def P4_Perm_Sync():
    
    permissions = P4GetProtect()
    perms = P4Perm_Parsing(permissions)

    NowDate = GetDate()

    coll_p4_perm.remove({})

    num=1
    for p in perms:

        doc = {'date': NowDate,
           'num': num,
           'accesslevel': p.AccessLevel,
           'type': p.Type,
           'name': p.Name,
           'host': p.Host,
           'folder': p.Folder,
           }

        coll_p4_perm.save(doc)
        num = num + 1

    return 'done!'





			
			
P4_Perm_Sync()			











