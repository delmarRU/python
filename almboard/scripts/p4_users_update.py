﻿import pymongo
import P4
from datetime import datetime
from pymongo import MongoClient
from bson.objectid import ObjectId
from P4 import *


Mongo_SRV = 'almboard'
Mongo_DB = 'almboard'
Mongo_Coll = 'P4_users'
Mongo_Coll_AD = 'AD_users'
Mongo_Col_P4_Perm = 'P4_permissions'
Mongo_Col_Mail = 'Mail_Log'
Mongo_Col_TFSManagers = 'TFS_managers'


conn = MongoClient(Mongo_SRV, 27017)
db = conn[Mongo_DB]
coll = db[Mongo_Coll]
coll_adusers = db[Mongo_Coll_AD]
coll_p4_perm = db[Mongo_Col_P4_Perm]

type_accounts = {'user': 'User',
                 'service': 'Service account',
                 'shared': 'Shared account',
                 'user_no_delete': 'User (not delete)'}

# Текущая дата
def GetDate():
    d = str(datetime.datetime.now())[:-7]
    return d

################################################################################
### Perforce
################################################################################

def P4GetAllUsers():
    p4 = P4()  # Create the P4 instance
    p4.user = "TFS_Integr_Dev"
    p4.client = "SHAKIROV-O"  # Set some environment variables
    p4.connect()

    res = p4.run('users')

    p4.disconnect()  # Disconnect from the server

    return res




def GetP4Users_ID(id):

    users = coll.find_one({"_id": ObjectId(id)})
    return users

# Поиск пользователя по account
def GetP4Users_Account(account):

    users = coll.find_one({"account": account})
    return users

# Активировать пользователя в MongoDB
#   id - ID-пользователя в MongoDB
#   flag принимает значения 1 - активировать (удалить дату удаления), 0 - деактивировать (установить дату удаления)
def GetP4Users_Activate(id,flag):

    # Активировать учётку
    if flag == 1:
        coll.update(
        {'_id': id},
            {'$set':
                 {'deleted_date': ''}
            }
        )

    # Деактивировать/удалить учётку
    else:
        deleted_date = GetDate()

        coll.update(
        {'_id': id},
        {'$set':
             {'deleted_date': deleted_date}
         }
        )


# Добавить новую P4-учётку в MongoDB
# P4Users_Add(account,name,mail,tfswi,comment,str(request.values.get('typeaccount')))
def P4Users_Add(account, name, mail, tfswi, comment, type_account):

    # Создание пользователя в Perforce
    P4CreateUser(account, name, mail)

    # Создание пользователя в Perforce
    P4Users_Add_Mongo(account, name, mail, tfswi, comment, type_account)

# Создание пользователя в Perforce
def P4Users_Add_Mongo(account, name, mail, tfswi, comment, type_account):

    NowDate = GetDate()

    user = GetP4Users_Account(account)

    # Если учётка уже существует
    if isinstance(user, dict):

        GetP4Users_Activate(user['_id'], 1)

        coll.update(
        {'_id': user['_id']},
        {'$set':
                {'name': name}

         }
        )

        coll.update(
        {'_id': user['_id']},
        {'$set':
                {'mail': mail}
         }
        )

        P4User_Comment_Add(user['_id'],"Учётая запись [" + account + "] повторно восстановлена (" + mail + ", " + name + ")!")

    else: # если не находим существующую учётку

        # Создаём новую учётку
        doc = {'account': account, 'name': name, 'mail': mail,
                'access': '',
                'change': '',
                'type_account': type_account,
                'created_date': NowDate,
                'deleted_date': '',
                'comment': '',
                'tfs_wi': tfswi
            }
        coll.save(doc)

        # Добавление комментария к новой созданной учётке
        us = GetP4Users_Account(account)
        P4User_Comment_Add(us['_id'],"Создана новая учётная запись [" + us['account'] + "] (" + us['mail'] + ", " + us['name'] + ")!")

    # если комментарий не пустой
    if comment != "":
        u = GetP4Users_Account(account)
        P4User_Comment_Add(u['_id'],comment)

def GetP4Users_All():
    users = coll.find({}).sort('account')
    return users

# Все активные (не удалённые) пользователи Perforce
def GetP4Users_Nondeleted():
    users = coll.find({"deleted_date": ""}).sort('account')
    return users

def P4User_Edit(id, type_account,comment,tfswi):

    for user in coll.find({"_id":ObjectId(id)}):
        ta = user["type_account"]
        ct = user["comment"]
        tf = user["tfs_wi"]

        if ta != type_account:
            coll.update({"_id": ObjectId(id)}, {"$set": {"type_account": type_account}})
        if ct != comment:
            P4User_Comment_Add(ObjectId(id), comment)
            #coll.update({"_id": ObjectId(id)}, {"$set": {"comment": comment}})
        if tf != tfswi:
            coll.update({"_id": ObjectId(id)}, {"$set": {"tfs_wi": tfswi}})

    return ''

# Добавить новый комментарий в History учётки
def P4User_Comment_Add(id, comment):
    # Поиск учётки по ID
    user = GetP4Users_ID(id)

    # текущая дата
    d = GetDate()

    comment_new = "[" + d + "]: \n" + comment + "\n\n" + user['comment']
    coll.update({'account': user['account']}, {"$set": {"comment": comment_new}})

# Обновить данные о последнем входе и последнем изменеии P4-учётки
def P4User_Update(id, access, update):

    coll.update({'account': id},
            {'$set':
                     {'access': access}
             }
            )

    coll.update({'account': id},
            {'$set':
                     {'change': update}
             }
            )



def Sync_P4Users_MongoDB():
    allusers = P4GetAllUsers()

    for u in allusers:
        c = coll.count({"account":u['User']})
        #    print(u['User']+ ' > '+ u['FullName'] + ' > '+ u['Email'] + ' > ' + str(datetime.datetime.fromtimestamp(int(u['Access']))) + ' > ' + str(datetime.datetime.fromtimestamp(int(u['Update']))))

        created_date = GetDate()

        if c == 0:
            P4Users_Add_Mongo(u['User'], u['FullName'], u['Email'], '', '', type_accounts['user'])

            us = GetP4Users_Account(u['User'])
            P4User_Update(us['_id'], str(datetime.datetime.fromtimestamp(int(u['Access']))), str(datetime.datetime.fromtimestamp(int(u['Update']))))

            #print(u['User']+ ' > '+ u['FullName'] + ' > '+ u['Email'] + ' > ' + str(datetime.datetime.fromtimestamp(int(u['Access']))) + ' > ' + str(datetime.datetime.fromtimestamp(int(u['Update']))))
            #doc = {'account': u['User'], 'name': u['FullName'], 'mail': u['Email'],
            #    'access': str(datetime.datetime.fromtimestamp(int(u['Access']))),
            #    'change': str(datetime.datetime.fromtimestamp(int(u['Update']))),
            #    'type_account': type_accounts['user'],
            #    'created_date': created_date,
            #    'deleted_date': ''}
            #coll.save(doc)

        elif c == 1:
            us = GetP4Users_Account(u['User'])
            P4User_Update(us['_id'], str(datetime.datetime.fromtimestamp(int(u['Access']))), str(datetime.datetime.fromtimestamp(int(u['Update']))))

            # Предусмотреть 2 случая:
            #   1- если учётка с признаком Deleted - тогда сообщить (comment) что учётка восстановлена, и очистить дату удаления
            #   2- если учётка без признака Deleted - обновить поля access и change и ничего не делать
        else:
            # если найденных учёток больше 1-й, то необходимо выполнить действие по активной учётке (deleted_date = "").
            print(c)



def Sync_MongoDB_P4Users():
    allusers = P4GetAllUsers()

    usersMongo = GetP4Users_Nondeleted()

    for c in usersMongo:       # c['account']
        temp = 0

        for a in allusers:      # a['User']
            if c['account'] == a['User']:
                temp = 1

        if temp == 0:
            deleted_date = GetDate()
            #coll.update({'account': c['account']}, {'$set': {'deleted_date': deleted_date}})

            P4User_Comment_Add(c['_id'], "Учетная запись [" + c['account'] + "] (" + c['mail'] + ", " + c['name'] + ") удалена!")
            GetP4Users_Activate(c['_id'], 0)
				
			
#P4_Perm_Sync()			
Sync_P4Users_MongoDB()
Sync_MongoDB_P4Users()










