﻿from pyad import *
from pyad.aduser import ADUser
import pyad.adquery

import pymongo
import P4
from datetime import datetime
from pymongo import MongoClient
from bson.objectid import ObjectId
from P4 import *


Mongo_SRV = 'almboard'
Mongo_DB = 'almboard'
Mongo_Coll = 'P4_users'
Mongo_Coll_AD = 'AD_users'

conn = MongoClient(Mongo_SRV, 27017)
db = conn[Mongo_DB]
coll = db[Mongo_Coll]
coll_adusers = db[Mongo_Coll_AD]


type_accounts = {'user': 'User',
                 'service': 'Service account',
                 'shared': 'Shared account',
                 'user_no_delete': 'User (not delete)'}

# Текущая дата
def GetDate():
    d = str(datetime.datetime.now())[:-7]
    return d

################################################################################
### Perforce
################################################################################

def P4GetAllUsers():
    p4 = P4()  # Create the P4 instance
    p4.user = "TFS_Integr_Dev"
    p4.client = "SHAKIROV-O"  # Set some environment variables
    p4.connect()

    res = p4.run('users')

    p4.disconnect()  # Disconnect from the server

    return res

def Sync_P4Users_MongoDB():
    allusers = P4GetAllUsers()

    for u in allusers:
        c = coll.count({"account":u['User']})
        #    print(u['User']+ ' > '+ u['FullName'] + ' > '+ u['Email'] + ' > ' + str(datetime.datetime.fromtimestamp(int(u['Access']))) + ' > ' + str(datetime.datetime.fromtimestamp(int(u['Update']))))

        created_date = GetDate()

        if c == 0:
            print(u['User']+ ' > '+ u['FullName'] + ' > '+ u['Email'] + ' > ' + str(datetime.datetime.fromtimestamp(int(u['Access']))) + ' > ' + str(datetime.datetime.fromtimestamp(int(u['Update']))))
            doc = {'account': u['User'], 'name': u['FullName'], 'mail': u['Email'],
                'access': str(datetime.datetime.fromtimestamp(int(u['Access']))),
                'change': str(datetime.datetime.fromtimestamp(int(u['Update']))),
                'type_account': type_accounts['user'],
                'created_date': created_date,
                'deleted_date': ''}
            coll.save(doc)
        elif c == 1:
            coll.update( {'account': u['User']},
                         {'$set':
                              {'access': str(datetime.datetime.fromtimestamp(int(u['Access'])))}
                          }
                         )
            coll.update( {'account': u['User']},
                         {'$set':                              
                              {'change': str(datetime.datetime.fromtimestamp(int(u['Update'])))}
                          }
                         )
        else:
            # если найденных учёток больше 1-й, то необходимо выполнить действие по активной учётке (deleted_date = "").
            print(c)


def Sync_MongoDB_P4Users():
    allusers = P4GetAllUsers()

    for c in coll.find():       # c['account']
        temp = 0

        for a in allusers:      # a['User']
            if c['account'] == a['User']:
                temp = 1

        if temp == 0:
            deleted_date = GetDate()
            
            coll.update(
                {"$and":
                     [
                         {'account': c['account']},
                         {'deleted_date': ''}
                     ]
                },
                {'$set':
                     {'deleted_date': deleted_date}
                 }
            )

###################################################################			
########### Active Directory
###################################################################
# Class AD-пользователя
class ADUser:
    def __init__(self,samaccountname,displayname='',mail='',position='',company='',country='',office='',division='',department='',manager='',mobile='',disabled=''):
        self.SamAccountName = samaccountname
        self.DisplayName = displayname
        self.Mail = mail
        self.Position = position
        self.Company = company
        self.Country = country
        self.Office = office
        self.Division = division
        self.Department = department
        self.Manager = manager
        self.Mobile = mobile
        self.Disabled = disabled # If Manager = '': Disabled = True


def GetManager(str):
    #CN=Denis Eremenko,OU=Users,OU=Russia,DC=avp,DC=ru
    #CN = Elena Andreeva, OU = Users, OU = Russia, DC = avp, DC = ru
    if str is None:
        Manager = ''
    else:
        end = str.find('OU')
        Manager = str[3:end - 1]

    return Manager

# Получить список пользователей
def GetADUsers_AD(name='*'):
    users = []
    q = pyad.adquery.ADQuery()

    #ou = 'DC=avp,DC=ru'

    OUs = ['OU=Users,OU=Russia,DC=avp,DC=ru','OU=Accounts,OU=Clients,DC=avp,DC=ru','OU=Leavers,OU=Clients,DC=avp,DC=ru','OU=ServiceAccounts,OU=Infra,DC=avp,DC=ru','OU=Accounts,OU=Hosting,DC=avp,DC=ru']
    #ou = 'OU=Users,OU=Russia,DC=avp,DC=ru'
    #ou = 'OU=Accounts,OU=Clients,DC=avp,DC=ru'
    #ou = 'OU=Leavers,OU=Clients,DC=avp,DC=ru' # уволенные
    #ou='OU=ServiceAccounts,OU=Infra,DC=avp,DC=ru' # сервисные
    #ou='OU=Accounts,OU=Hosting,DC=avp,DC=ru' # сервисные
    attr = ["DisplayName","SamAccountName","mail","Title","Company","co","StreetAddress","Division","Department","Manager","mobile"]
    mac = " and SamAccountName <> '*1*'  and SamAccountName <> '*2*'  and SamAccountName <> '*3*' and SamAccountName <> '*4*' and SamAccountName <> '*5*' and SamAccountName <> '*6*' and SamAccountName <> '*7*' and SamAccountName <> '*8*' and SamAccountName <> '*9*' and SamAccountName <> '*0*'"
    filtr = " and objectClass = 'user' " + mac

    #and SamAccountName <> '*$*' "
    #memberOf = 'Domain users'

    for ou in OUs:
        # Все пользователи
        if name == '*':
            q.execute_query(
                attributes = attr,
                where_clause = "SamAccountName = '*' " + filtr,
                base_dn = ou
            )
        # поиск по вхождению (name)
        else:
            q.execute_query(
                attributes=attr,  # "userPrincipalName","Name","DisplayName"],
                # where_clause = "objectClass = 'user'",
                # where_clause = "userPrincipalName = 'Shakirov_O@avp.ru'",
                # where_clause = "SamAccountName = 'Moshkarin'",
                where_clause="SamAccountName = '*" + name + "*' "+filtr,
                base_dn=ou
            )



        for row in q.get_results():

            manager = GetManager(row["Manager"])
            #svc_
            if str(row["SamAccountName"])[:4] == 'svc_':
                disabled = ""
            elif manager == '':
                disabled = "True"
            else:
                disabled = "False"

           # print(row)
            #print(disabled)

            user = ADUser(samaccountname=row["SamAccountName"],displayname=row["DisplayName"],mail=row["mail"],position=row["Title"],company=row["Company"],country=row["co"],office=row["StreetAddress"],division=row["Division"],department=row["Department"],manager=manager,mobile=row["mobile"],disabled=disabled)
            users.append(user)

    return users

def GetADUsers_All():
    users = coll_adusers.find({}).sort('samaccountname')
    return users
	
def ADUsers_Add(aduser):
    NowDate = GetDate()

    doc = {'changedate': NowDate,
           'samaccountname': aduser.SamAccountName,
           'displayname': aduser.DisplayName,
           'mail': aduser.Mail,
           'position': aduser.Position,
           'company': aduser.Company,
           'country': aduser.Country,
           'office': aduser.Office,
           'division': aduser.Division,
           'department': aduser.Department,
           'manager': aduser.Manager,
           'mobile': aduser.Mobile,
           'disabled': aduser.Disabled
    }
    coll_adusers.save(doc)


# Синхронизировать MongoDB с AD (список AD-пользователей)
def Sync_AD(name='*'):
    db_users = GetADUsers_All()

    if name=='*':
        ad_users = GetADUsers_AD()
    else:
        ad_users = GetADUsers_AD(name)

    for u in ad_users:
        c = coll_adusers.count({'samaccountname': u.SamAccountName})
        u_db = coll_adusers.find_one({"samaccountname": u.SamAccountName})

        # Нет учётки, нужно добавить новую
        if c == 0:
            ADUsers_Add(u)
            #print(u)

        # есть 1 учётка, нужно проверить все поля на актуальность
        elif c == 1:

            flag = 0


            # Проверка полей
            if u_db["displayname"] != u.DisplayName:
                flag = 1

                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'displayname': u.DisplayName}
                            }
                            )

            if u_db["mail"] != u.Mail:
                flag = 1
                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'mail': u.Mail}
                            }
                            )

            if u_db["position"] != u.Position:
                flag = 1
                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'position': u.Position}
                            }
                            )

            if u_db["company"] != u.Company:
                flag = 1
                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'company': u.Company}
                            }
                            )

            if u_db["country"] != u.Country:
                flag = 1
                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'country': u.Country}
                            }
                            )

            if u_db["office"] != u.Office:
                flag = 1
                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'office': u.Office}
                            }
                            )

            if u_db["division"] != u.Division:
                flag = 1
                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'division': u.Division}
                            }
                            )

            if u_db["department"] != u.Department:
                flag = 1
                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'department': u.Department}
                            }
                            )

            if u_db["manager"] != u.Manager:
                flag = 1
                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'manager': u.Manager}
                            }
                            )

            if u_db["mobile"] != u.Mobile:
                flag = 1
                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'mobile': u.Mobile}
                            }
                            )

            if u_db["disabled"] != u.Disabled:
                flag = 1
                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'disabled': u.Disabled}
                            }
                            )

            if flag ==1:
                NowDate = GetDate()
                coll_adusers.update( {'samaccountname': u.SamAccountName},
                             {'$set':
                                  {'changedate': NowDate}
                            }
                            )
        else:
            # если найденных учёток больше 1-й, то необходимо выполнить действие по активной учётке (deleted_date = "").
            print(c)
					

Sync_P4Users_MongoDB()
Sync_MongoDB_P4Users()
Sync_AD()









