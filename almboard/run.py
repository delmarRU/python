import os
from almboard import create_app

#app = create_app(os.getenv('FLASK_CONFIG') or 'test')


#app = create_app('test')
app = create_app('prod')

if __name__ == '__main__':
    #app.run()
    #app.run(host='127.0.0.1', port=5020)
    app.run(host='0.0.0.0', port=5020)
