class TfsProject:

    # инициация проекта
    # name, manager, description, tfs_template, tfs_wi, archived, comment
    # _id, created_date, deleted_date, update_date
    def __init__(self, name):
        self.name = name

    # Обновление проекта
    def update(self):
        pass

    ##################################################################
    # Свойства
    ##################################################################

    # manager / Менеджер
    @property
    def manager(self): return self._manager
    @manager.setter
    def manager(self, value): self._manager = value

    # description / Описание проекта
    @property
    def description(self): return self._description
    @description.setter
    def description(self, value): self._description = value

    def __str__(self):
        return 'TFS-проект "%s" ' % self.name

tfs_project = TfsProject('AAA')
#tfs_project.manager = 'bbb'

#print(tfs_project.__str__())



