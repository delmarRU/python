from flask import render_template, request
from almboard.tfs import tfs
from almboard.tfs.module import *

################################################################
#
# TFS Projects
#
################################################################

# Список всех проектов
@tfs.route('/tfs-projects')
def tfs_projects ():
    users = tfs_projects_get_all()
    return render_template('/tfs/tfs-projects.html',fl='',todos=users) #,t=title,h=heading)

@tfs.route('/tfs_projects_action', methods=['POST'])
def tfs_projects_action ():

    nondeleted = request.form.get('flag')
    if nondeleted == 'True':
        users = tfs_projects_get_notarchived_notmanager()
        flag = 'checked'
    else:
        users = tfs_projects_get_all()
        flag = ''

    return render_template('/tfs/tfs-projects.html',todos=users,fl=flag) #t=title,h=heading)

@tfs.route('/tfs-projects-add')
def tfs_projects_add ():
    return render_template('/tfs/tfs-projects-add.html')

#
@tfs.route("/tfs-projects-add-action", methods=['POST'])
def tfs_projects_add_action ():

    name=str(request.values.get('name'))
    manager=str(request.values.get('manager'))
    description=str(request.values.get('description'))
    tfstemplate=request.values.get('tfstemplate')
    tfswi = str(request.values.get('tfswi'))
    comment = str(request.values.get('comment'))
    archived = str(request.values.get('archived'))

    tfs_project_add(name,manager,description,tfstemplate,tfswi,archived,comment)

    users = tfs_projects_get_all().sort('access')


    project = tfs_project_get(name)
    tfstemplate = project['tfstemplate']

    #return render_template('tfs-projects.html',todos=users,t=title,h=heading)
    return render_template('/tfs/tfs-projects-edit.html', project=project, tfst=tfstemplate)

@tfs.route("/tfs-projects-edit")
def tfs_projects_edit ():

    id = request.args.get("_id")
    project = tfs_project_get_id(id)
    tfstemplate = project['tfstemplate']

    if project['archived'] == "Yes":
        check = 'checked'
    else:
        check = ''
    #p4u = p4_users_edit_F(id)

    return render_template('/tfs/tfs-projects-edit.html',project=project,tfst=tfstemplate,ch=check)

# Редактирование пользователя, кнопка "Update"
@tfs.route("/tfs-projects-edit-action", methods=['POST'])
def tfs_projects_edit_action():

    id = request.values.get('_id')
    name = request.values.get('name')
    manager = request.values.get('manager')
    description = request.values.get('description')
    tfstemplate = request.values.get('tfstemplate')
    tfswi = request.values.get('tfswi')
    archived = str(request.form.get('archived'))

    comment = request.values.get('comment')

    check_control = request.form.get('archived')
    if check_control == 'Yes':
        check = 'checked'
        archived = 'Yes'
    else:
        check = ''
        archived = 'No'

    #print(archived)

    tfs_project_edit(id, name, manager, description, str(tfstemplate), tfswi, archived, comment)

    project = tfs_project_get_id(id)
    tfstemplate = project['tfstemplate']

    return render_template('/tfs/tfs-projects-edit.html', project=project, tfst=tfstemplate,ar=archived, ch=check)
