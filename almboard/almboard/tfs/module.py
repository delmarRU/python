from config import Config
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import timedelta, datetime

CFG = Config()

conn = MongoClient(CFG.MONGODB_HOST, CFG.MONGODB_PORT)
db = conn[CFG.MONGODB_DB]

coll_tfs_projects = db[CFG.MONGODB_COL_TFS_PROJECTS]

# Изменить дату обновления проекта
def tfs_project_update(id):
    # текущая дата
    now_date = str(datetime.now())[:-7]
    coll_tfs_projects.update({"_id": ObjectId(id)}, {"$set": {"update_date": now_date}})

    return ''

# Добавить новый проект
# Пример: TFS_Projects_Add('ADG','Timofey Kazakov')
# TFSManagers_Add(project,manager,description,tfstemplate,tfswi,comment)
def tfs_project_add(name,manager,description,tfstemplate,tfswi,archived,comment):
    now_date = str(datetime.now())[:-7]

    doc = {
            'created_date': now_date,
            'deleted_date': '',
            'update_date': now_date,
            'name': name,
            'manager': manager,
            'description': description,
            'tfstemplate': tfstemplate,
            'tfswi': tfswi,
            'archived': archived,
            'comment': ''
           }

    coll_tfs_projects.save(doc)

    project = tfs_project_get(name)
    id = project['_id']

    comments = "NEW PROJECT CREATED:\n"
    comments = comments + "name = [" + name + "];\n"
    comments = comments + "manager = [" + manager + "];\n"
    comments = comments + "description = [" + description + "];\n"
    comments = comments + "tfstemplate = [" + tfstemplate + "];\n"
    comments = comments + "tfswi = [" + tfswi + "];\n"
    comments = comments + "archived = [" + archived + "];\n"

    tfs_project_comment_add(ObjectId(id), comments, 1)

    if comment != '':
        tfs_project_comment_add(ObjectId(id), comment, 0)

    return ''

# Добавить новый комментарий в History учётки
# Auto = 0 - комментарий от пользователя
# Auto = 1 - комментарий от робота
def tfs_project_comment_add(id, comment, Auto):
    # Поиск проекта по ID
    project = tfs_project_get_id(id)

    # текущая дата
    d = str(datetime.now())[:-7]

    if Auto == 0:
        comment_new = "[" + d + "]: \n" + str(comment) + "\n\n" + str(project['comment'])
    else:
        comment_new = "[" + d + "]: *** \n" + str(comment) + "\n\n" + str(project['comment'])
        tfs_project_update(id)

    coll_tfs_projects.update({"_id": ObjectId(id)}, {"$set": {"comment": comment_new}})

# показать все проекты
def tfs_projects_get_all():
    res = coll_tfs_projects.find() #.sort('date', -1)
    return res

# показать без менеджера и не архивные
def tfs_projects_get_notarchived_notmanager():
    res = coll_tfs_projects.find({ '$and': [ {'manager': ''}, {'archived': 'No'} ] })

    return res

#a = TFSProjects_GetNotArchivedNotManager()
#for i in a:
#    print(i)

# Поиск проекта по ID
def tfs_project_get_id(id):

    res = coll_tfs_projects.find_one({"_id": ObjectId(id)})
    return res

    #id = ObjectId("59e48fa89e85f4292520175d")

# Поиск проекта по имени
def tfs_project_get(name):

    res = coll_tfs_projects.find_one({'name': name})
    return res

# Очистить всю таблицу TFS Projects
def tfs_project_remove():
    coll_tfs_projects.remove({})

# Изменить информацию о проекте
def tfs_project_edit(id,name,manager,description,tfstemplate,tfswi,archived,comment):

    for project in coll_tfs_projects.find({"_id":ObjectId(id)}):
        nm = project["name"]
        ma = project["manager"]
        de = project["description"]
        tt = project["tfstemplate"]
        tw = project["tfswi"]
        ar = project["archived"]

        comments_title = "CHANGE FIELDS:\n"
        comments = ""

        if nm != name:
            coll_tfs_projects.update({"_id": ObjectId(id)}, {"$set": {"name": name}})
            if comments != "":
                comments = comments +"\n"
            comments = comments + "name = ["+name+"];"
        if ma != manager:
            coll_tfs_projects.update({"_id": ObjectId(id)}, {"$set": {"manager": manager}})
            if comments != "":
                comments = comments +"\n"
            comments = comments + "manager = ["+manager+"];"
        if de != description:
            coll_tfs_projects.update({"_id": ObjectId(id)}, {"$set": {"description": description}})
            if comments != "":
                comments = comments +"\n"
            comments = comments + "description = ["+description+"];"
        if tt != tfstemplate:
            coll_tfs_projects.update({"_id": ObjectId(id)}, {"$set": {"tfstemplate": tfstemplate}})
            if comments != "":
                comments = comments +"\n"
            comments = comments + "tfstemplate = ["+tfstemplate+"];"
        if tw != tfswi:
            coll_tfs_projects.update({"_id": ObjectId(id)}, {"$set": {"tfswi": tfswi}})
            if comments != "":
                comments = comments +"\n"
            comments = comments + "tfswi = ["+tfswi+"];"
        if ar != archived:
            coll_tfs_projects.update({"_id": ObjectId(id)}, {"$set": {"archived": archived}})
            if comments != "":
                comments = comments +"\n"
            comments = comments + "archived = ["+archived+"];"
        if comment != '':
            tfs_project_comment_add(ObjectId(id), comment, 0)

        if comments != "":
            tfs_project_comment_add(ObjectId(id), comments_title+comments, 1)

    return ''
