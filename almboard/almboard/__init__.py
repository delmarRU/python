from flask import Flask, render_template, url_for, render_template_string, redirect
from flask_bootstrap import Bootstrap
from flask_mail import Mail, Message
from config import config

bootstrap = Bootstrap()
mail = Mail()

def create_app(config_name):

    app = Flask(__name__)
    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    bootstrap.init_app(app) # Bootstrap(app)
    mail.init_app(app)

    from almboard.ad import ad as ad_blueprint
    from almboard.api import api as api_blueprint
    from almboard.main import main as main_blueprint
    from almboard.mailo import mailo as mailo_blueprint
    from almboard.perforce import perforce as perforce_blueprint
    from almboard.tfs import tfs as tfs_blueprint

    app.register_blueprint(ad_blueprint, url_prefix='/ad')
    app.register_blueprint(api_blueprint, url_prefix='/api')
    app.register_blueprint(main_blueprint)
    app.register_blueprint(mailo_blueprint, url_prefix='/mail')
    app.register_blueprint(perforce_blueprint, url_prefix='/perforce')
    app.register_blueprint(tfs_blueprint, url_prefix='/tfs')

    return app
