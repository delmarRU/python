from flask import render_template, request, redirect
from almboard.perforce import perforce
from almboard.perforce.module import *

############################################
# Mail
############################################
@perforce.route('/p4-users')
def p4users ():
    users = GetP4Users_Nondeleted() #.sort('access')
    check = 'checked'
    #onclick="this.form.submit();"

    # return render_template('/perforce/p4-users.html')
    return render_template('/perforce/p4-users.html',todos=users,ch=check)

@perforce.route('/p4-users-candidats-for-deletion')
def p4_users_candidats_for_deletion ():
    users = GetP4Users_Candidates_for_Deleted()
    count = users.count()
    return render_template('/perforce/p4-users.html',todos=users,c=count)

@perforce.route('/add-user')
def maillog():
    return render_template('/perforce/add-user.html')


def p4_users_edit_F(id):
    users = GetP4Users_ID(id)
    type_account = users["type_account"]
    ta = "User"
    if type_account == "User (not delete)":
        ta = "US_N"
    if type_account == "User (only CodeCollab)":
        ta = "US_C"
    if type_account == "Service account":
        ta = "SRV"
    if type_account == "Shared account":
        ta = "SHD"
    if type_account == "Deleted":
        ta = "DEL"

    perms = GetP4Permissions_Account(users["account"]).sort('num')

    return {'user': users, 'ta': ta, 'perms': perms}

@perforce.route("/p4-users-edit")
def p4_users_edit ():

    id = request.args.get("_id")
    p4u = p4_users_edit_F(id)

    return render_template('/perforce/p4-users-edit.html',user=p4u['user'],ta=p4u['ta'],todos=p4u['perms'])


@perforce.route('/p4_users_action', methods=['POST'])
def p4_users_action ():

    nondeleted = request.form.get('checkbox')
    if nondeleted == 'True':
        users = GetP4Users_Nondeleted().sort('access')
        check = 'checked'
    else:
        users = GetP4Users_All().sort('access')
        check = ''

    return render_template('/perforce/p4-users.html',todos=users,ch=check)

# Редактирование пользователя, кнопка "Update"
@perforce.route("/p4-users-edit-action", methods=['POST'])
def p4_users_edit_action():

    id = request.values.get('id')

    type_account = request.values.get('typeaccount')
    comment = request.values.get('comment')
    tfswi = request.values.get('tfswi')

    P4User_Edit(id, str(type_account),comment,tfswi)
    p4u = p4_users_edit_F(id)

    return render_template('/perforce/p4-users-edit.html', user=p4u['user'], ta=p4u['ta'], todos=p4u['perms'])

@perforce.route('/p4-users-add')
def p4_users_add ():
    if "_id" in request.args:
        id = request.args.get("_id")
        user = GetADUsers_ID(id)
        samaccountname = user['samaccountname']
        fullname = user['displayname']
        mail = user['mail']

    else:
        samaccountname = ''
        fullname = ''
        mail = ''

    # количество свободных лицензий Perforce
    FreeLicense = GetP4Users_FreeLicense()

    return render_template('/perforce/p4-users-add.html',sam=samaccountname,fn=fullname,mail=mail,free=FreeLicense)

@perforce.route("/p4-users-add-action", methods=['POST'])
def p4_users_add_action ():
    #Adding a Task
    account=request.values.get('account')
    name=request.values.get('name')
    mail=request.values.get('mail')
    tfswi = request.values.get('tfswi')
    comment = request.values.get('comment')
    P4Users_Add(account,name,mail,tfswi,comment,str(request.values.get('typeaccount')))

    return redirect('/perforce/p4-users')

@perforce.route("/p4-users-reset-psw")
def p4_users_reset_psw ():

    newPSW = 'StartStart'
    #id = request.values.get("_id")
    id = request.args.get("_id")
    user = GetP4Users_ID(id)
    account = user['account']

    # изменение пароля
    P4PswChange(account, newPSW)

    text = '''     <p style="font-family: &quot;Calibri Light&quot;, sans-serif;"><span style="background-color: transparent;">Здравствуйте!</span><br></p><p style=""><font face="Calibri Light, sans-serif">Для вашего аккаунта Perforce <strong>''' + account + '''</strong> установлен пароль</font></p><p style=""><font face="Calibri Light, sans-serif"> <strong>'''+ newPSW + '''</strong> (необходимо поменять)</font><br></p> 

                        <p style="font-family: &quot;Calibri Light&quot;, sans-serif;"><strong>ALM Support</strong><br>
                        Internal Tools Development | Kaspersky Lab<br>
                        E-mail: <a href="mailto:almsupport@kaspersky.com"><span style="color: windowtext;">almsupport@kaspersky.com</span></a></p>                                            
                        '''

    # отправка письма
    SendMail('almsupport@kaspersky.com', [user['mail']], ['oleg.shakirov@kaspersky.com'], 'Пароль Perforce', text)

    return render_template('/perforce/p4-users-reset-psw.html',account=account)

@perforce.route('/p4-permissions')
def p4_permissions ():

    if "account" in request.args:
        account = request.args.get("account")
        perms = GetP4Permissions_Account(account).sort('num')

    else:
        perms = GetP4Permissions().sort('num')

    return render_template('/perforce/p4-permissions.html',todos=perms)




