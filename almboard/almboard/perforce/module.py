from config import Config
from datetime import timedelta, datetime
from pymongo import MongoClient
from bson.objectid import ObjectId

# mail #######################################
import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
##############################################

from P4 import *
import sys
import re

CFG = Config()

mail_server = CFG.MAIL_SERVER


perforce_srv = CFG.PERFORCE_SRV
perforce_host = CFG.PERFORCE_HOST
perforce_port = CFG.PERFORCE_PORT
perforce_user = CFG.PERFORCE_USER
perforce_psw = CFG.PERFORCE_PSW
perforce_client = CFG.PERFORCE_CLIENT
perforce_licenses = CFG.PERFORCE_LICENSES # количество купленных лицензий Perforce

conn = MongoClient(CFG.MONGODB_HOST, CFG.MONGODB_PORT)
db = conn[CFG.MONGODB_DB]

coll_p4_users = db[CFG.MONGODB_COL_P4_USERS]
coll_p4_perm = db[CFG.MONGODB_COL_P4_PERM]
coll_ad_users = db[CFG.MONGODB_COL_AD_USERS]

# Типы учёток Perforce
type_accounts = {'user': 'User',
                 'service': 'Service account',
                 'shared': 'Shared account',
                 'user_codecollab': 'User (only CodeCollab)',
                 'user_no_delete': 'User (not delete)'}


def SendMail(mail_from, mail_to, mail_bcc, mail_subj, mail_text):

    server = smtplib.SMTP(mail_server)

    server.set_debuglevel(1)
    server.ehlo()

    msg = MIMEMultipart()


    #####################################################
    msg['From'] = mail_from
    msg['To'] = ', '.join(mail_to)
    msg['Bcc'] = ', '.join(mail_bcc)
    msg['Subject'] = mail_subj
    body = mail_text
    #####################################################

    msg.attach(MIMEText(body, 'html', 'utf8'))
    text = msg.as_string()

    mail_all = mail_to + mail_bcc
    server.sendmail(mail_from, mail_all, text)

    server.quit()

# Текущая дата минус какое-то количество дней
def GetDateMinusDays(days):
    date_minus = str(datetime.datetime.today() + timedelta(days=-days))[:-7]
    return date_minus

#a= GetDateMinusDays(3)
#print(a)


# Все активные (не удалённые) пользователи Perforce
def GetP4Users_Nondeleted():
    users = coll_p4_users.find({"deleted_date": ""}).sort('account')
    return users

def GetP4Users_Candidates_for_Deleted():
    #Генерация даты 3 месяца назад
    days = 61
    date_3month_ago = GetDateMinusDays(days)
    users = coll_p4_users.find({"$and": [{"type_account": type_accounts['user']}, {"access": {"$lt": date_3month_ago}}, {"deleted_date": {"$eq": ""}} ]}).sort('account')
    return users

# Поиск пользователя по ID
def GetP4Users_ID(id):

    users = coll_p4_users.find_one({"_id": ObjectId(id)})
    return users

# Получить список прав из MongoDB для определённого аккаунта
def GetP4Permissions_Account(account):
    perms = coll_p4_perm.find({"name": account})
    return perms


def Perforce():

        p4 = P4()  # Create the P4 instance

        p4.client = perforce_client # "SHAKIROV-O"  # Set some environment variables
        p4.port = perforce_srv
        p4.user = perforce_user  # "TFS_Integr_Dev"
        p4.password = perforce_psw

        p4.connect()

        return res
 
'''
# Получение списка всех пользователей в Perforce
def P4GetAllUsers():
    p4 = P4()  # Create the P4 instance
    p4.user = perforce_user  # "TFS_Integr_Dev"
    p4.client = perforce_client # "SHAKIROV-O"  # Set some environment variables
#    p4.host = perforce_host
    p4.port = perforce_srv
    p4.password = perforce_psw
    p4.connect(

    res = p4.run('users')

    p4.disconnect()  # Disconnect from the server

    return res

'''

def GetP4Users_All():
    users = coll_p4_users.find({}).sort('account')
    return users

def P4User_Comment_Add(id, comment):
    # Поиск учётки по ID
    user = GetP4Users_ID(id)

    # текущая дата
    d = str(datetime.datetime.now())[:-7]

    comment_new = "[" + d + "]: \n" + comment + "\n\n" + user['comment']
    coll_p4_users.update({'account': user['account']}, {"$set": {"comment": comment_new}})

# Обновить P4-учетку (тип учетки и комментарий)
#Пример: P4User_Edit('597749bf9e85f43c63a541e8','User (not delete)','коммент')
def P4User_Edit(id, type_account,comment,tfswi):

    for user in coll_p4_users.find({"_id":ObjectId(id)}):
        ta = user["type_account"]
        ct = user["comment"]
        tf = user["tfs_wi"]

        if ta != type_account:
            coll_p4_users.update({"_id": ObjectId(id)}, {"$set": {"type_account": type_account}})
            P4User_Comment_Add(ObjectId(id), "Тип аккаунта изменён на ["+type_account+"].")
        if comment != '':
            P4User_Comment_Add(ObjectId(id), comment)
            #coll.update({"_id": ObjectId(id)}, {"$set": {"comment": comment}})
        if tf != tfswi:
            coll_p4_users.update({"_id": ObjectId(id)}, {"$set": {"tfs_wi": tfswi}})
            P4User_Comment_Add(ObjectId(id), "Изменено поле TFS_wi на [" + tfswi + "].")

    return ''


# Поиск пользователя AD по _id
def GetADUsers_ID(id):
    users = coll_ad_users.find_one({"_id": ObjectId(id)})
    return users

# Количество свободных лицензий Perforce
def GetP4Users_FreeLicense():
    free = perforce_licenses - GetP4Users_Nondeleted_Count()
    return free

# Количество активных пользователей Perforce
def GetP4Users_Nondeleted_Count():
    users_count = coll_p4_users.find({"deleted_date": ""}).count()
    return users_count



# Поиск пользователя по account
def GetP4Users_Account(account):

    users = coll_p4_users.find_one({"account": account})
    return users

# Активировать пользователя в MongoDB
#   id - ID-пользователя в MongoDB
#   flag принимает значения 1 - активировать (удалить дату удаления), 0 - деактивировать (установить дату удаления)
def GetP4Users_Activate(id,flag):

    # Активировать учётку
    if flag == 1:
        coll_p4_users.update(
        {'_id': id},
            {'$set':
                 {'deleted_date': ''}
            }
        )

    # Деактивировать/удалить учётку
    else:
        deleted_date = str(datetime.datetime.now())[:-7]

        coll_p4_users.update(
        {'_id': id},
        {'$set':
             {'deleted_date': deleted_date}
         }
        )


# Добавить новую P4-учётку в MongoDB
# P4Users_Add(account,name,mail,tfswi,comment,str(request.values.get('typeaccount')))
def P4Users_Add(account, name, mail, tfswi, comment, type_account):

    # Создание пользователя в Perforce
    P4CreateUser(account, name, mail)

    # Создание пользователя в Perforce
    P4Users_Add_Mongo(account, name, mail, tfswi, comment, type_account)

# Создание пользователя в Perforce
def P4Users_Add_Mongo(account, name, mail, tfswi, comment, type_account):

    NowDate = str(datetime.datetime.now())[:-7]

    user = GetP4Users_Account(account)

    # Если учётка уже существует
    if isinstance(user, dict):

        GetP4Users_Activate(user['_id'], 1)

        coll_p4_users.update(
        {'_id': user['_id']},
        {'$set':
                {'name': name}

         }
        )

        coll_p4_users.update(
        {'_id': user['_id']},
        {'$set':
                {'mail': mail}
         }
        )

        P4User_Comment_Add(user['_id'],"Учётая запись [" + account + "] повторно восстановлена (" + mail + ", " + name + ")!")

    #SendMail(mail_from, mail_to, mail_bcc, mail_subj, mail_text):

    else: # если не находим существующую учётку

        # Создаём новую учётку
        doc = {'account': account, 'name': name, 'mail': mail,
                'access': '',
                'change': '',
                'type_account': type_account,
                'created_date': NowDate,
                'deleted_date': '',
                'comment': '',
                'tfs_wi': tfswi
            }
        coll_p4_users.save(doc)

        # Добавление комментария к новой созданной учётке
        us = GetP4Users_Account(account)
        P4User_Comment_Add(us['_id'],"Создана новая учётная запись [" + us['account'] + "] (" + us['mail'] + ", " + us['name'] + ")!")

    text = '''     <p style="font-family: &quot;Calibri Light&quot;, sans-serif;"><span style="background-color: transparent;">Здравствуйте!</span><br></p><p style=""><font face="Calibri Light, sans-serif">Для вас создана учётная запись Perforce <strong>''' + account + '''</strong>.&nbsp;</font></p><p style=""><font face="Calibri Light, sans-serif">Пароль: <strong>StartStart</strong> (необходимо поменять)</font><br></p> 
                        <br>
                        <p style="font-family: &quot;Calibri Light&quot;, sans-serif;"><strong>ALM Support</strong><br>
                        Internal Tools Development | Kaspersky Lab<br>
                        E-mail: <a href="mailto:almsupport@kaspersky.com"><span style="color: windowtext;">almsupport@kaspersky.com</span></a></p>                                            
                        '''

    SendMail('almsupport@kaspersky.com', [mail], ['oleg.shakirov@kaspersky.com'], 'Создана учётная запись Perforce', text)



    # если комментарий не пустой
    if comment != "":
        u = GetP4Users_Account(account)
        P4User_Comment_Add(u['_id'],comment)

###################################################################
#
# Права доступа / p4 protect
#
###################################################################

# Получить список всех прав из MongoDB
def GetP4Permissions():
    perms = coll_p4_perm.find({})
    return perms

# Получить список прав из MongoDB для определённого аккаунта
def GetP4Permissions_Account(account):
    perms = coll_p4_perm.find({"name": account})
    return perms

###################################################################
#
# Perforce API
#
###################################################################

# Получение списка всех пользователей в Perforce
def P4GetAllUsers():
    p4 = P4()  # Create the P4 instance

    p4.client = perforce_client # "SHAKIROV-O"  # Set some environment variables
    p4.port = perforce_srv
    p4.user = perforce_user  # "TFS_Integr_Dev"
    p4.password = perforce_psw

    p4.connect()

    res = p4.run('users')

    p4.disconnect()  # Disconnect from the server

    return res

# Проверить пользователя на существование
# 0 - не найден, 1 - найден
def P4UserValidate(account):
    p4 = P4()  # Create the P4 instance

    p4.client = perforce_client # "SHAKIROV-O"  # Set some environment variables
    p4.port = perforce_srv
    p4.user = perforce_user  # "TFS_Integr_Dev"
    p4.password = perforce_psw

    res = 0
    try:
        p4.connect()
        output = p4.run('users', account)
        if len(output)>0:
            res = 1
        #print(len(output))
        p4.disconnect()

    except P4Exception:
        for e in p4.errors:  # Display errors
            print(e)

    return res

# Изменения пароля для пользователя user
def P4PswChange(user, psw):
    p4 = P4()  # Create the P4 instance

    p4.client = perforce_client # "SHAKIROV-O"  # Set some environment variables
    p4.port = perforce_srv
    p4.user = perforce_user  # "TFS_Integr_Dev"
    p4.password = perforce_psw
    p4.connect()

    p4.run('passwd', '-P', psw, user)

    p4.disconnect()  # Disconnect from the server

# Добавить пользователя в P4-группу
def P4UserAddGroup(account, group):
    p4 = P4()  # Create the P4 instance

    p4.client = perforce_client # "SHAKIROV-O"  # Set some environment variables
    p4.port = perforce_srv
    p4.user = perforce_user  # "TFS_Integr_Dev"
    p4.password = perforce_psw

    p4.connect()

    spec_group=p4.run('group', "-o", group)
    # spec_group = [{'Group': 'test', 'MaxResults': 'unset', 'MaxScanRows': 'unset', 'MaxLockTime': 'unset', 'Timeout': '43200', 'PasswordTimeout': 'unset', 'Users': ['test3', 'test5', 'test7']}]

    spec_group[0]['Users'].append(account)

    p4.save_group(spec_group, "-i")

    p4.disconnect()  # Disconnect from the server

# Создать нового пользователя (с установкой пароля 'StartStart' и добавлением в группу 'everyone')
# 1 - создан ; 0 - не создан
def P4CreateUser(account, fullname, mail):
    res = 0
    p4 = P4()  # Create the P4 instance

    p4.client = perforce_client # "SHAKIROV-O"  # Set some environment variables
    p4.port = perforce_srv
    p4.user = perforce_user  # "TFS_Integr_Dev"
    p4.password = perforce_psw

    # Проверяем пользователя на существование
    if P4UserValidate(account) == 0:

        p4.connect()

        newuser = {
            'User': account,
            'Email': mail,
            'FullName': fullname
        }
        p4.save_user(newuser, "-f")
        P4UserAddGroup(account, 'everyone')
        P4PswChange(account, 'StartStart')

        p4.disconnect()  # Disconnect from the server
        res = 1
    return res


# Получить список прав
def P4GetProtect():
    p4 = P4()  # Create the P4 instance

    p4.client = perforce_client # "SHAKIROV-O"  # Set some environment variables
    p4.port = perforce_srv
    p4.user = perforce_user  # "TFS_Integr_Dev"
    p4.password = perforce_psw

    p4.connect()

    spec=p4.run('protect', "-o")
    p4.disconnect()  # Disconnect from the server

    return spec

