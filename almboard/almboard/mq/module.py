from random import randint
import pika
# https://www.programcreek.com/python/example/9241/pika.ConnectionParameters

###########################################
MQ_SERVER = 'hqrndsrv-t4'
MQ_PORT = 5672
MQ_DIR = '/'
MQ_USER = 'almboard'
MQ_PSW = 'almboard'

MQ_EXCHANGE = ''
###########################################

class MQ:

        def SendQueue(queue, key, message):
            credentials = pika.PlainCredentials(MQ_USER, MQ_PSW)
            parameters = pika.ConnectionParameters(MQ_SERVER, MQ_PORT, MQ_DIR, credentials)
            connection = pika.BlockingConnection(parameters)

            channel = connection.channel()
            channel.queue_declare(queue=queue)  # создание очереди 'hello'

            channel.basic_publish(exchange=MQ_EXCHANGE,
                                  routing_key=key,
                                  body=message)

            connection.close()

            return 0

        def ReceiveQueue(self,queue):
            credentials = pika.PlainCredentials(MQ_USER, MQ_PSW)
            parameters = pika.ConnectionParameters(MQ_SERVER, MQ_PORT, MQ_DIR, credentials)
            connection = pika.BlockingConnection(parameters)

            channel = connection.channel()
            channel.queue_declare(queue=queue)

            print(' [*] Waiting for messages. To exit press CTRL+C')

            def callback(ch, method, properties, body):
                print (" [x] Received %r" % (body,))

            channel.basic_consume(callback,
                                  queue=queue,
                                  no_ack=True)

            channel.start_consuming()


#a = MQ()

#b = a.ReceiveQueue('hello')
#print(b)

#for i in range(10):
#    a.SendQueue('test_queue', 'message', str(randint(100, 999)))