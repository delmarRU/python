from config import Config
from datetime import timedelta, datetime
from pymongo import MongoClient
from bson.objectid import ObjectId

from flask_mail import Message

import smtplib
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart

CFG = Config()

conn = MongoClient(CFG.MONGODB_HOST, CFG.MONGODB_PORT)
db = conn[CFG.MONGODB_DB]

coll_mail_log = db[CFG.MONGODB_COL_MAIL_LOG]

mail_server = CFG.MAIL_SERVER #'mailinternal.avp.ru'

##########################################################
#
# Классы
#
##########################################################

class Mail:

    sender = ''
    subj = ''
    text = ''
    to = []     # получатели TO
    bcc = []    # получатели BCC

    def __init__(self, sender = '', subject = '', text = ''):
        self.subject = subject
        self.text = text
        self.sender = sender

    # Отправить письмо
    def send(self, sender=sender, to=to, bcc=bcc, subj=subj, text=text):
            server = smtplib.SMTP(mail_server)

            server.set_debuglevel(1)
            server.ehlo()

            msg = MIMEMultipart()

            #####################################################
            msg['From'] = sender
            msg['To'] = ', '.join(to)
            msg['Bcc'] = ', '.join(bcc)
            msg['Subject'] = subj
            body = text
            #####################################################

            msg.attach(MIMEText(body, 'html', 'utf8'))
            text = msg.as_string()

            mail_all = to + bcc
            server.sendmail(sender, mail_all, text)

            server.quit()

    # получить список всех оповещений
    @staticmethod
    def get_log(self):
        pass

    # Проверка, готово ли письмо к отправке
    # Т.е. заполнены все необходимые поля
    def validate(self):
        pass
        #if subject
        #subject

mail1 = Mail('oleg.shakirov@kaspersky.com','test message1','''<p style="font-family: 'Calibri Light',sans-serif;">
                    <p>Уважаемые Коллеги,</p>
                    
                    <p>В настоящий момент наблюдаются проблемы в работе следующих сервисов:</p>
                     
                    
                    <p>Наши сотрудники работают над решением проблемы, и мы сообщим Вам, когда она будет устранена.</p>
                    
                    <p style="text-align: center;" align="center"><hr align="center" size="2" width="100%"></p>
                    
                    <p>Dear Colleagues,<br><br>At the moment the provision of the following services is unstable:</p>
                    
                     
                    
                    <p>We are working on the issue and will inform you when the problem is fixed.</p>
                    
                    <p><strong>ALM Support</strong><br>
                    Internal Tools Development | Kaspersky Lab<br>
                    E-mail: <a href="mailto:almsupport@kaspersky.com"><span style="color: windowtext;">almsupport@kaspersky.com</span></a></p>
                    </p>
                    ''')


#mail1.subject= 'Aaaa1'
#mail1.to.append('oleg.shakirov@kaspersky.com')
#mail1.bcc.append('oleg.shakirov@kaspersky.com')
#mail.add_bcc('oleg.shakirov1@kaspersky.com')
#print(mail1.bcc)
#mail1.send()