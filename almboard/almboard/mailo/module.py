import string

from config import Config
from datetime import timedelta, datetime
from pymongo import MongoClient
from bson.objectid import ObjectId
import smtplib
from smtplib import SMTPException, SMTPAuthenticationError
from email.mime.text import MIMEText
from email.mime.image import MIMEImage
from email.mime.multipart import MIMEMultipart
from random import randint


# import base64
#from ntlm_auth.ntlm.

# from ntlm_auth.ntlm




CFG = Config()

conn = MongoClient(CFG.MONGODB_HOST, CFG.MONGODB_PORT)
db = conn[CFG.MONGODB_DB]

coll_mail_log = db[CFG.MONGODB_COL_MAIL_LOG]
coll_p4_users = db[CFG.MONGODB_COL_P4_USERS]

mail_server = CFG.MAIL_SERVER #'mailinternal.avp.ru'
mail_user = CFG.MAIL_USER
mail_psw = CFG.MAIL_PSW
#mail_server = 'smtp.avp.ru'
#mail_port = 587



############################################

# Типы учёток Perforce
type_accounts = {'user': 'User',
                 'service': 'Service account',
                 'shared': 'Shared account',
                 'user_codecollab': 'User (only CodeCollab)',
                 'user_no_delete': 'User (not delete)'}

############################################

# def asbase64(msg):
#  # encoding the message then convert to string
#  return((base64.b64encode(msg)).decode("utf-8"))

def ntlm_authenticate(smtp, username, password):

    code, response = smtp.docmd("AUTH", "NTLM " +  .asbase64(Ntlm.create_negotiate_message(username)))
    if code != 334:
        raise SMTPException("Server did not respond as expected to NTLM negotiate message")
    challenge, flags = ntlm.parse_challenge_message(str(base64.decodebytes(response)))
    user_parts = username.split("\\", 1)
    code, response = smtp.docmd("", Ntlm.asbase64(Ntlm.create_negotiate_message(challenge, user_parts[1], user_parts[0], password, flags)))
    if code != 235:
        raise SMTPAuthenticationError(code, response)

def GetMailLog_All():
    logs = coll_mail_log.find() #.sort('date', -1)
    return logs

def mailGenerateDate(date_start, time):

    mydate = datetime(date_start['year'],date_start['month'],date_start['day'])

    date_format = ('0' if date_start['day'] < 10 else '') + str(date_start['day'])+'.'+('0' if date_start['month'] < 10 else '') +str(date_start['month'])+'.'+str(date_start['year'])
    month =  mydate.strftime("%B")

    m = {1:'января', 2:'февраля', 3:'марта', 4: 'апреля', 5: 'мая', 6: 'июня', 7: 'июля', 8: 'августа', 9: 'сентября', 10: 'октября', 11: 'ноября', 12: 'декабря'}

    # Окончания английских дат
    day_ending = ''
    if date_start['day'] in [1, 21, 31]:
        day_ending = 'st'
    if date_start['day'] in [2, 22]:
        day_ending = 'nd'
    if date_start['day'] in [3, 23]:
        day_ending = 'rd'
    if date_start['day'] in [4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,24,25,26,27,28,29,30]:
        day_ending = 'th'

    # Английское время 12ч ФОРМАТ AM:PM
    def AMPM(hour):
        hour_t = ''
        type_t = ''


        if hour == 0:
            hour_t = 12
            type_t = 'AM'
        elif hour > 0 and hour < 12:
            hour_t = hour
            type_t = 'AM'
        elif hour == 12:
            hour_t = hour
            type_t = 'PM'
        elif hour > 12:
            hour_t = hour-12
            type_t = 'PM'

        res = {'hour': hour_t, 'type': type_t}

        return res

    date_en = month + ' ' + str(date_start['day'])
    date_ru = str(date_start['day']) +' '+ m[date_start['month']]

    # Чтобы вместо 17:3 выводилось 17:03
    if time['minute_start'] < 10:
        minute_start = '0'+ str(time['minute_start'])
    else:
        minute_start = str(time['minute_start'])
    if time['minute_finish'] < 10:
        minute_finish = '0'+ str(time['minute_finish'])
    else:
        minute_finish = str(time['minute_finish'])

    time_ru = ' с '+('0' if time['hour_start'] < 10 else '')+  str(time['hour_start']) + ':' + minute_start + ' до ' +('0' if time['hour_finish'] < 10 else '')+ str(time['hour_finish']) + ':' + minute_finish

    hour_start = AMPM(time['hour_start'])
    hour_finish = AMPM(time['hour_finish'])
    time_en = ' from '+ str(hour_start['hour']) + ':' + minute_start + ' ' + hour_start['type'] + ' to ' + str(hour_finish['hour']) + ':' + minute_finish + ' ' + hour_finish['type']

    res = {'date_ru': date_ru, 'date_en': date_en, 'day_ending': day_ending, 'time_ru': time_ru, 'time_en': time_en, 'date_format': date_format}

    return res

# Генерация списка сервисов в HTML
def mailGenerateHTMLService(services):
    text = ''
    if len(services) > 0:
        text = '<ul style="margin-top: 0cm;" type="disc">'
        for serv in services:
            text = text+ '<li><span style="color:#0070c0"><strong><em>'+serv+'</em></strong></span></li>'
        text = text + '</ul>'

    return text

# Получить шаблон письма
def mailGetTemplate(type, services, date_start, time):

    date = mailGenerateDate(date_start,time)

    servicesHTML = mailGenerateHTMLService(services)

    #mail_from, mail_to, mail_bcc, mail_subj, mail_text
    # date = {'date_ru': date_ru, 'date_en': date_en, 'day_ending': day_ending, 'time_ru': date_ru, 'time_en': date_en}

    mail_bcc_test = '''ALM@kaspersky.com;
    oleg.shakirov@kaspersky.com;
    '''

    mail_to_general = 'alm@kaspersky.com'
    mail_bcc_general = '''StaffAutoGroup_1317@kaspersky.com; 
    StaffAutoGroup_418@kaspersky.com; 
    StaffAutoGroup_758@kaspersky.com; 
    StaffAutoGroup_2659@kaspersky.com; 
    StaffAutoGroup_2033@kaspersky.com; 
    StaffAutoGroup_2658@kaspersky.com; 
    StaffAutoGroup_369@kaspersky.com; 
    StaffAutoGroup_2660@kaspersky.com; 
    StaffAutoGroup_2664@kaspersky.com; 
    StaffAutoGroup_377@kaspersky.com; 
    StaffAutoGroup_2661@kaspersky.com; 
    StaffAutoGroup_2662@kaspersky.com; 
    ext_bogaturev@kaspersky.com; 
    StaffAutoGroup_2663@kaspersky.com; 
    ext_kulman@kaspersky.com; 
    StaffAutoGroup_2657@kaspersky.com; 
    StaffAutoGroup_3212@kaspersky.com; 
    StaffAutoGroup_2665@kaspersky.com; 
    StaffAutoGroup_2667@kaspersky.com; 
    StaffAutoGroup_1144@kaspersky.com; 
    Legal@kaspersky.com; 
    quest@kaspersky.com; 
    StaffAutoGroup_911@kaspersky.com; 
    StaffAutoGroup_2569@kaspersky.com; 
    TISAdmin@kaspersky.com; 
    karmafrerrors@kaspersky.com'''

    mail_bcc_general2 = '''Anti-Malware Research, Research & Development <StaffAutoGroup_1317@kaspersky.com>; 
        Architecture Office, Research & Development <StaffAutoGroup_418@kaspersky.com>; 
        Cloud & Content Technologies Research, Research & Development <StaffAutoGroup_758@kaspersky.com>; 
        Consumer Product Line, Research & Development <StaffAutoGroup_2659@kaspersky.com>; 
        Content Filtering  Research, Research & Development <StaffAutoGroup_2033@kaspersky.com>; 
        Core Technologies Product Line, Research & Development <StaffAutoGroup_2658@kaspersky.com>; 
        Design & Usability, Research & Development <StaffAutoGroup_369@kaspersky.com>; 
        Endpoint Protection Product Line, Research & Development <StaffAutoGroup_2660@kaspersky.com>; 
        Fraud Prevention Product Line, Research & Development <StaffAutoGroup_2664@kaspersky.com>; 
        Global Research & Analysis Team (GReAT), Research & Development <StaffAutoGroup_377@kaspersky.com>; 
        KPC Product Line, Research & Development <StaffAutoGroup_2661@kaspersky.com>; 
        Mac Product Line, Research & Development <StaffAutoGroup_2662@kaspersky.com>; 
        Maxim Bogaturev (Offsite EXT) <ext_bogaturev@kaspersky.com>; 
        Mobile Product Line, Research & Development <StaffAutoGroup_2663@kaspersky.com>; 
        Oleg Kulman (Offsite EXT) <ext_kulman@kaspersky.com>; 
        R&D BackOffice, Research & Development <StaffAutoGroup_2657@kaspersky.com>; 
        Security Services, Research & Development <StaffAutoGroup_3212@kaspersky.com>; 
        Technology Solutions Product Line, Research & Development <StaffAutoGroup_2665@kaspersky.com>; 
        Web&Messaging Product Line, Research & Development <StaffAutoGroup_2667@kaspersky.com>; 
        Whitelisting & Cloud Research, Research & Development <StaffAutoGroup_1144@kaspersky.com>; 
        Legal <Legal@kaspersky.com>; 
        Quest <quest@kaspersky.com>; 
        Future Technologies <StaffAutoGroup_911@kaspersky.com>; 
        Corporate Products Expert Support, Technical Support Excellence <StaffAutoGroup_2569@kaspersky.com>; 
        TISAdmin <TISAdmin@kaspersky.com>; 
        karmafrerrors@kaspersky.com'''

    if type == 'incident':

        mail_from = 'almsupport@kaspersky.com'
        mail_to = mail_to_general
        mail_bcc = mail_bcc_general

        mail_subj = '[ALM] Problem with service'
        mail_text = '''<div style="font-family: 'Calibri Light',sans-serif;">
                    <p>Уважаемые Коллеги,</p>
                    <p></p>
                    <p>В настоящий момент наблюдаются проблемы в работе следующих сервисов:</p>
                    '''+servicesHTML+''' 
                    <p></p>
                    <p>Наши сотрудники работают над решением проблемы, и мы сообщим Вам, когда она будет устранена.</p>
                    <p></p>
                    <div style="text-align: center;" align="center"><hr align="center" size="2" width="100%" /></div>
                    <p></p>
                    <p>Dear Colleagues,<br /><br />At the moment the provision of the following services is unstable:</p>
                    <p></p>
                    '''+servicesHTML+''' 
                    <p></p>
                    <p>We are working on the issue and will inform you when the problem is fixed.</p>
                    <p></p>
                    <p><strong>ALM Support</strong><br>
                    Internal Tools Development | Kaspersky Lab<br>
                    E-mail: <a href="mailto:almsupport@kaspersky.com"><span style="color: windowtext;">almsupport@kaspersky.com</span></a></p>
                    </div>
                    '''

    ###############################################################################
    elif type == 'test':
        mail_from = 'oleg.shakirov@kaspersky.com'
        mail_to = 'Oleg <oleg.shakirov@kaspersky.com>; Vitaly <vitaly.ivanovsky@kaspersky.com>; ALEX <alexey.s.petrov@kaspersky.com>'
        #mail_to = 'alexey.s.petrov@kaspersky.com'
        #mail_to = 'oleg.shakirov@kaspersky.com, alexey.s.petrov@kaspersky.com'
        mail_bcc = ''
        mail_subj = 'Test ALM mail-service ' + str(randint(100, 999))
        mail_text = '''<div style="font-family: 'Calibri Light',sans-serif;">
                    <p>Уважаемый Коллега,</p>
                    <p></p>
                    <p>Это тестовое письмо, просьба проигнорировать.</p>
                    <p></p>
                    <p><strong>ALM Support</strong><br>
                    Internal Tools Development | Kaspersky Lab<br>
                    E-mail: <a href="mailto:almsupport@kaspersky.com"><span style="color: windowtext;">almsupport@kaspersky.com</span></a></p>
                    </div>
                    '''

    ###############################################################################
    elif type == 'maintenance':

        mail_from = 'almsupport@kaspersky.com'
        mail_to = mail_to_general
        mail_bcc = mail_bcc_general #mail_bcc_test
        mail_subj = '[ALM] Maintenance works on '+ date['date_en']+date['day_ending']
        mail_text = '''<div style="font-family: 'Calibri Light',sans-serif;">
                    <p>Уважаемые Коллеги,</p>
                    <p></p>
                    <p><strong><em><span style="color: #0070c0;">''' + date['date_ru'] + ''' ''' + date['time_ru'] + '''</span></em></strong>&nbsp;по московскому времени будут проводится планово-профилактические работы следующих сервисов:</p>
                    '''+servicesHTML+''' 
                    <p></p>
                    <p>В указанное время сервисы будут недоступны. Если необходима работоспособность перечисленных сервисов, ответьте на это письмо с указанием причины.</p>
                    <p></p>
                    <div style="text-align: center;" align="center"><hr align="center" size="2" width="100%" /></div>
                    <p></p>
                    <p>Dear Colleagues,<br /><br />We are informing you about maintenance works on following services, planned for <strong><em><span style="color: #0070c0;">''' + date['date_en'] + '''<sup>''' + date['day_ending'] + '''</sup>&nbsp;''' + date['time_en'] + '''</span></em></strong>, Moscow time:</p>
                    <p></p>
                    '''+servicesHTML+''' 
                    <p></p>
                    <p>The services will be unavailable at this time. If you need these services to be available, then reply to this message, and indicate the reason.</p>
                    <p></p>
                    <p><strong>ALM Support</strong><br>
                    Internal Tools Development | Kaspersky Lab<br>
                    E-mail: <a href="mailto:almsupport@kaspersky.com"><span style="color: windowtext;">almsupport@kaspersky.com</span></a></p>
                    </div>
                    '''


    elif type == 'rcm':
        mail_from = 'almsupport@kaspersky.com'
        mail_to = 'Stanislav.Sidristy@kaspersky.com; Semen.Ivanov@kaspersky.com; oleg.shakirov@kaspersky.com'
        mail_bcc = '''Ekaterina.Smirnova@kaspersky.com; Victoria.Shipenkova@kaspersky.com; Andrey.Rubin@kaspersky.com; Alexey.Semenov@kaspersky.com; Alexandra.Kufeld@kaspersky.com; Lyudmila.Bataeva@kaspersky.com;
            Vladimir.Bochkov@kaspersky.com;
            Antonina.Boyarkova@kaspersky.com;
            Yanina.Radzishevskaya@kaspersky.com;
            Vyacheslav.Samarin@kaspersky.com;
            Andrei.Mochola@kaspersky.com;
            Olga.Postnikova@kaspersky.com;
            Stanislav.Sidristy@kaspersky.com;
            Denis.Lisitsyn@kaspersky.com;
            Alexey.Chikov@kaspersky.com;
            Alexander.Ermakovich@kaspersky.com;
            Roman.Bozhko@kaspersky.com;
            Vladimir.Kukushkin@kaspersky.com;
            Alexander.Rassokhin@kaspersky.com;
            Maria.Chernyshova@kaspersky.com;
            Alexander.Onishchenko@kaspersky.com;
            Alexey.Klimenchuk@kaspersky.com;
            Konstantin.Voronkov@kaspersky.com;
            Evgeniya.Shirobokova@kaspersky.com;
            Pavel.Nechaev@kaspersky.com;
            Sergey.Gridnev@kaspersky.com;
            Marina.Zykova@kaspersky.com;
            Karina.Napadovskaya@kaspersky.com;
            Alexandra.Kalchenko@kaspersky.com;
            Andrey.Khudyakov@kaspersky.com;
            Sergey.Abramov@kaspersky.com;
            Yana.Valatskayte@kaspersky.com;
            Elena.Petrova@kaspersky.com;
            Anton.Nikitenkov@kaspersky.com;
            Victor.Yablokov@kaspersky.com;
            Oleg.Bykov@kaspersky.com;
            Dmitry.Aleshin@kaspersky.com;
            sergey.politsyn@kaspersky.com;
            Alexey.S.Petrov@kaspersky.com;
            Christina.Butorina@kaspersky.com;
            Alexander.Konovalenko@kaspersky.com;
            Ekaterina.Zarovnaya@kaspersky.com;
            Elena.Smirnova@infowatch.com;
            Arsen.Adamyan@kaspersky.com;
            Sergey.Safronov@kaspersky.com;
            Alexander.Bosov@kaspersky.com;
            Ilya.Efimov@kaspersky.com;
            Marina.Sobolevskaya@kaspersky.com;
            Vitaly.Zaitsev@kaspersky.com;
            Karen.Galstyan@kaspersky.com;
            Alexander.Prokhin@kaspersky.com;
            Anatoly.Simonenko@kaspersky.com;
            Timur.Smirnov@kaspersky.com;
            Denis.Tkachenko@kaspersky.com;
            Ekaterina.Uchaeva@kaspersky.com;
            Alexander.Safonov@kaspersky.com;
            Anna.Blagikh@kaspersky.com;
            Maria.Bologova@kaspersky.com;
            Vyacheslav.Presnyakov@kaspersky.com;
            Alexey.Lebedev@kaspersky.com;
            Dmitry.Lukasevich@kaspersky.com;
            Oleg.Mitichkin@kaspersky.com;
            Anton.Agafonov@kaspersky.com;
            Maxim.Petrov@kaspersky.com;
            Alena.Zharikova@kaspersky.com;
            Vladimir.Kardash@kaspersky.com;
            Elena.Kharchenko@kaspersky.com;
            Alexander.Maskaev@kaspersky.com;
            Ekaterina.Samargina@kaspersky.com;
            Oleg.Nevstruev@kaspersky.com;
            Sergey.Belistov@kaspersky.com;
            Oleg.Shakirov@kaspersky.com;
            Pavel.Mezhuev@kaspersky.com;
            Artem.Serebrov@kaspersky.com;
            Evgeny.Myachenkov@kaspersky.com;
            Elena.Liberman@kaspersky.com;
            Denis.Eremenko@kaspersky.com;
            Petr.Aleshkin@kaspersky.com;
            Dmitry.Levchenko@kaspersky.com;
            Evgeny.Semenov@kaspersky.com;
            Petr.Filippov@kaspersky.com;
            Ivan.Vassunov@kaspersky.com;
            Evgeny.Zverev@kaspersky.com;
            Ekaterina.Shamsutdinova@kaspersky.com;
            Tatyana.Chubchenkova@kaspersky.com;
            Timur.Amirkhanov@kaspersky.com;
            Vasily.Lutsky@kaspersky.com;
            Vasily.Lutsky@kaspersky.com;
            Dmitry.Lukiyan@kaspersky.com;
            Svetlana.Mushta@kaspersky.com;
            Renat.Turyanov@kaspersky.com;
            TT_TFS_Syncer@kaspersky.com;
            Tatyana.Mukhina@kaspersky.com;
            Dmitry.Filinsky@kaspersky.com;
            Denis.Chernov@kaspersky.com;
            Marina.Smolyanaya@kaspersky.com;
            Timofey.Titkov@kaspersky.com;
            Ivan.Rozhdestvensky@kaspersky.com;
            Kirill.Suvorov@kaspersky.com;
            Mikhail.Bogdanovich@kaspersky.com;
            Vladimir.Bondarenko@kaspersky.com;
            Boris.Fomenko@kaspersky.com;
            Ivan.Kutepov@kaspersky.com;
            Anatoly.Chernov@kaspersky.com;
            Semen.Ivanov@kaspersky.com;
            Dantsan.Zhamsoev@kaspersky.com;
            Stanislav.Nikolaev@kaspersky.com;
            Sergey.Dobrovolsky@kaspersky.com;
            Timur.Biyachuev@kaspersky.com;
            elena.evseeva@kaspersky.com;
            irina.potanina@kaspersky.com;
            Alexey.Misyagin@kaspersky.com;
            Madina.Mukhamedova@kaspersky.com;
            Leonid.Granovsky@kaspersky.com;
            Anton.Selikhov@kaspersky.com;
            Bathyr.Shikhmuradov@kaspersky.com;
            Yuliya.Pronina@kaspersky.com;
            Yury.Rakov@kaspersky.com;
            Anna.Kapitanova@kaspersky.com;
            Ekaterina.Krasilnikova@kaspersky.com;
            Anton.Mefodichev@kaspersky.com'''

        mail_subj = '[ALM] Maintenance works on ' + date['date_en'] + date['day_ending']

        mail_text = '''<div style="font-family: 'Calibri Light',sans-serif;">
                        <p>Уважаемые Коллеги,</p>
                        <p></p>
                        <p><strong><em><span style="color: #0070c0;">''' + date['date_ru'] + ''' ''' + date['time_ru'] + '''</span></em></strong>&nbsp;по московскому времени будут проводится планово-профилактические работы следующих сервисов:</p>
                         <ul><li><span style="color:#0070c0"><strong><em>RCM (<a href="https://rcm.avp.ru/">https://rcm.avp.ru</a>)</em></strong></span></li></ul> 
                        <p></p>
                        <p>В указанное время сервисы будут недоступны. Если необходима работоспособность перечисленных сервисов, ответьте на это письмо с указанием причины.</p>
                        <p></p>
                        <div style="text-align: center;" align="center"><hr align="center" size="2" width="100%" /></div>
                        <p></p>
                        <p>Dear Colleagues,<br /><br />We are informing you about maintenance works on following services, planned for <strong><em><span style="color: #0070c0;">
                        ''' + date['date_en'] + '''<sup>''' + date['day_ending'] + '''</sup>&nbsp;''' + date['time_en'] + '''</span></em></strong>, Moscow time:</p>
                        <p></p>
                        <ul><li><span style="color:#0070c0"><strong><em>RCM (<a href="https://rcm.avp.ru/">https://rcm.avp.ru</a>)</em></strong></span></li></ul> 
                        <p></p>
                        <p>The services will be unavailable at this time. If you need these services to be available, then reply to this message, and indicate the reason.</p>
                        <p></p>
                        <p><strong>ALM Support</strong><br>
                        Internal Tools Development | Kaspersky Lab<br>
                        E-mail: <a href="mailto:almsupport@kaspersky.com"><span style="color: windowtext;">almsupport@kaspersky.com</span></a></p>
                        </div>
                        '''

    elif type == 'perforce':

        mail_from = 'almsupport@kaspersky.com'
        mail_to = 'oleg.shakirov@kaspersky.com; denis.eremenko@kaspersky.com; vitaly.ivanovsky@kaspersky.com'

        mail_bcc = GetP4Users_Candidates_Deleted_Mail()
        mail_subj = '[ALM] Деактивация учётных записей Perforce / Perforce account deactivation notice'
        mail_text = '''<div style="font-family: 'Calibri Light',sans-serif;">
                    <p>Уважаемый Коллега,</p>
                    <p></p>
                    <p>Уведомляем вас, что по итогам инвентаризации и анализа пользовательской активности ваша учетная запись в системе Perforce запланирована для деактивации <strong><em><font color="red">''' + date['date_format'] + '''</font></strong></em></p>
                    <div>
                    <hr /></div>
                    <p>Dear Colleague!</p>
                    <p></p>
                    <p>Please be informed, that according to the last Perforce user activity inventory results, your Perforce account is marked for deletion at <strong><em><font color="red">''' + date['date_format'] + '''</font></em></strong>.</p>                    
                    <p></p>
                    </span>
                    <p><strong>ALM Support</strong><br>
                        Internal Tools Development | Kaspersky Lab<br>
                        E-mail: <a href="mailto:almsupport@kaspersky.com"><span style="color: windowtext;">almsupport@kaspersky.com</span></a></p>
                        </div>'''

    elif type == 'perforce2':

        mail_from = 'oleg.shakirov@kaspersky.com'
        mail_to = ''
        mail_bcc = GetP4Users_Candidates_Deleted_Mail()
        mail_subj = 'Удаление Perforce-учётки'
        mail_text = '''<span style="font-family:tahoma,geneva,sans-serif">
                    <p>Привет!</p>
                    <p></p>                                    
                    <p>По нашим данным, твоя Perforce-учётка давно не используется. Она тебе требуется или можем её удалить? </p>                    
                    <p>Если требуется, напиши вкратце для чего используется, я помечу в нашем реестре, чтобы учётку не удаляли.</p>
                    <p></p>
                    <p>На Perforce закончились свободные лицензии, в связи с этим ищем неиспользуемые Perforce-учётки для их удаления.</p>
                    </span> '''

    res = {'mail_from': mail_from, 'mail_to': mail_to, 'mail_bcc': mail_bcc, 'mail_subj': mail_subj, 'mail_text': mail_text}

    return res


# Текущая дата минус какое-то количество дней
def GetDateMinusDays(days):
    date_minus = str(datetime.today() + timedelta(days=-days))[:-7]
    return date_minus


def GetP4Users_Candidates_for_Deleted():
    #Генерация даты 3 месяца назад
    days = 61
    date_3month_ago = GetDateMinusDays(days)
    users = coll_p4_users.find({"$and": [{"type_account": type_accounts['user']}, {"access": {"$lt": date_3month_ago}}, {"deleted_date": {"$eq": ""}} ]}).sort('account')
    return users


# Получить email всех кандидатов на удаление из Perforce
def GetP4Users_Candidates_Deleted_Mail():
    users = GetP4Users_Candidates_for_Deleted()

    res=''
    for u in users:
        res=res+u['mail']+'; '

    return res

#def ADUsers_Add(changedate, samaccountname, displayname, mail, position, company, country, office, division, department, manager, mobile, disabled):
#SendMail_LogAdd(mail_fr, mail_to, mail_bc, mail_sj, mail_tx)
def SendMail_LogAdd(m_from, m_to, m_bcc, m_subject, m_text):
    NowDate = str(datetime.now())[:-7]

    doc = {'date': NowDate,
           'from': m_from,
           'to': m_to,
           'bcc': m_bcc,
           'subject': m_subject,
           'text': m_text
    }

    coll_mail_log.save(doc)

def SendMail(mail_from, mail_to, mail_bcc, mail_subj, mail_text):

    server = smtplib.SMTP(mail_server, 587)

    server.set_debuglevel(1)
    server.ehlo()

    msg = MIMEMultipart()


    #####################################################
    msg['From'] = mail_from
    msg['To'] = ', '.join(mail_to)
    msg['Bcc'] = ', '.join(mail_bcc)
    msg['Subject'] = mail_subj
    body = mail_text
    #####################################################

    msg.attach(MIMEText(body, 'html', 'utf8'))
    text = msg.as_string()

    mail_all = mail_to + mail_bcc

    ntlm_authenticate(server, r"KL\almsupport", mail_psw)
    #server.login('almsupport@kaspersky.com', mail_psw)
    server.sendmail(mail_from, mail_all, text)

    #print(mail_all)
    #['alm@kaspersky.com', 'StaffAutoGroup_1317@kaspersky.com', '     Architecture Office, Research & Development <StaffAutoGroup_418@kaspersky.com>', '     Cloud & Content Technologies Research, Research & Development <StaffAutoGroup_758@kaspersky.com>', '     Consumer Product Line, Research & Development <StaffAutoGroup_2659@kaspersky.com>', '     Content Filtering  Research, Research & Development <StaffAutoGroup_2033@kaspersky.com>', '     Core Technologies Product Line, Research & Development <StaffAutoGroup_2658@kaspersky.com>', '     Design & Usability, Research & Development <StaffAutoGroup_369@kaspersky.com>', '     Endpoint Protection Product Line, Research & Development <StaffAutoGroup_2660@kaspersky.com>', '     Fraud Prevention Product Line, Research & Development <StaffAutoGroup_2664@kaspersky.com>', '     Global Research & Analysis Team (GReAT), Research & Development <StaffAutoGroup_377@kaspersky.com>', '     KPC Product Line, Research & Development <StaffAutoGroup_2661@kaspersky.com>', '     Mac Product Line, Research & Development <StaffAutoGroup_2662@kaspersky.com>', '     Maxim Bogaturev (Offsite EXT) <ext_bogaturev@kaspersky.com>', '     Mobile Product Line, Research & Development <StaffAutoGroup_2663@kaspersky.com>', '     Oleg Kulman (Offsite EXT) <ext_kulman@kaspersky.com>', '     R&D BackOffice, Research & Development <StaffAutoGroup_2657@kaspersky.com>', '     Security Services, Research & Development <StaffAutoGroup_3212@kaspersky.com>', '     Technology Solutions Product Line, Research & Development <StaffAutoGroup_2665@kaspersky.com>', '     Web&Messaging Product Line, Research & Development <StaffAutoGroup_2667@kaspersky.com>', '     Whitelisting & Cloud Research, Research & Development <StaffAutoGroup_1144@kaspersky.com>', '     Legal <Legal@kaspersky.com>', '     Quest <quest@kaspersky.com>', '     Future Technologies <StaffAutoGroup_911@kaspersky.com>', '     Corporate Products Expert Support, Technical Support Excellence <StaffAutoGroup_2569@kaspersky.com>', '     TISAdmin <TISAdmin@kaspersky.com>', '     karmafrerrors@kaspersky.com']
    #['Oleg <oleg.shakirov@kaspersky.com>', '   ALEX <alexey.s.petrov@kaspersky.com>', '']
    #smtplib.SMTPRecipientsRefused: {'     Architecture Office, Research & Development <oleg.shakirov@kaspersky.com>': (501, b'5.1.3 Invalid address'), '': (501, b'5.1.3 Invalid address')}

    server.quit()

    return 'Сообщение c темой "' + mail_subj + '" отправлено получателям!'



#a = mailGetTemplate('test', [],{'year': 2017, 'month': 10, 'day': 3}, {'hour_start': 12, 'minute_start': 30, 'hour_finish': 14, 'minute_finish': 45})

