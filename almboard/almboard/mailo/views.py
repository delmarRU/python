from flask import render_template, request
from almboard.mailo import mailo
from almboard.mailo.module import *

############################################
# Mail
############################################
@mailo.route('/mail-send')
def send_mail ():

    notification_type='incident'

    message = mailGetTemplate(notification_type,
                    [],
                    {'year': 2017, 'month': 1, 'day': 1},
                    {'hour_start': 12, 'minute_start': 00, 'hour_finish': 14, 'minute_finish': 00}
                    )


    from_t = message['mail_from']
    to_t=message['mail_to']
    bcc_t=message['mail_bcc']
    subj_t = message['mail_subj']
    text_t=message['mail_text']

    return render_template('/mail/mail-send.html',fr=from_t,to=to_t,bc=bcc_t,sj=subj_t,tx=text_t, status = "")

@mailo.route('/mail-log')
def maillog():
    logs = GetMailLog_All().sort('date', -1)

    return render_template('/mail/mail-log.html',todos=logs)

@mailo.route("/mail_send_action", methods=['POST'])
def mail_send_action():

    from_t = ''
    to_t = ''
    bcc_t = ''
    subj_t = ''
    text_t = ''
    notift_t = ''
    status = ''
    print(request.form['btn'])

    if request.form['btn'] == 'GENERATE':


        notification_type = request.values.get('notificationtype')  # incident / maintenance / recover / rcm
        # print(nt)

        services = request.form.getlist('services')

        date_d = request.values.get('date_d')
        date_m = request.values.get('date_m')
        date_y = request.values.get('date_y')
        time_h = request.values.get('time_h')
        time_m = request.values.get('time_m')

        time_h_f = request.values.get('time_h_f')
        time_m_f = request.values.get('time_m_f')

        # mailGenerateDate(date_start, time)
        # mailGenerateDate({'year': date_y, 'month': date_m, 'day': date_d}, {'hour_start': time_h, 'minute_start': time_m,'hour_finish': time_h_f, 'minute_finish': time_m_f})

        message = mailGetTemplate(notification_type,
                                  services,
                                  {'year': int(date_y), 'month': int(date_m), 'day': int(date_d)},
                                  {'hour_start': int(time_h), 'minute_start': int(time_m), 'hour_finish': int(time_h_f),
                                   'minute_finish': int(time_m_f)}
                                  )

        from_t = message['mail_from']
        to_t = message['mail_to']
        bcc_t = message['mail_bcc']
        subj_t = message['mail_subj']
        text_t = message['mail_text']
        notift_t = notification_type
        status = ''

    elif request.form['btn'] == 'SEND':
        mail_fr = request.values.get('mail_from')
        mail_to = request.values.get('mail_to')
        mail_bc = request.values.get('mail_bcc')
        mail_sj = request.values.get('mail_subj')
        mail_tx = request.values.get('mail_text')

        from_t = mail_fr
        to_t = mail_to
        bcc_t = mail_bc
        subj_t = mail_sj
        text_t = mail_tx
        notift_t = request.values.get('notificationtype')  # incident / maintenance / recover / rcm / perforce

        status = SendMail(mail_fr, mail_to.split(';'), mail_bc.split(';'), mail_sj, mail_tx)

        # Сохранить сообщение в логе
        SendMail_LogAdd(mail_fr, mail_to, mail_bc, mail_sj, mail_tx)


    return render_template('/mail/mail-send.html',to=to_t,fr=from_t,bc=bcc_t,sj=subj_t,tx=text_t,nt=notift_t, status = status)

