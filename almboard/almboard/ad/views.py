from flask import render_template, request
from almboard.ad import ad
from almboard.ad.module import *


@ad.route('/ad-users')
def adusers():
    users = GetADUsers_All()

    return render_template('/ad/ad-users.html',todos=users)