from config import Config
from pymongo import MongoClient
from bson.objectid import ObjectId
from datetime import timedelta, datetime

CFG = Config()

conn = MongoClient(CFG.MONGODB_HOST, CFG.MONGODB_PORT)
db = conn[CFG.MONGODB_DB]

coll_ad_users = db[CFG.MONGODB_COL_AD_USERS]


def GetADUsers_All():
    users = coll_ad_users.find({}).sort('samaccountname')
    return users

def GetADUsers_ID(id):

    users = coll_ad_users.find_one({"_id": ObjectId(id)})
    return users

#def ADUsers_Add(changedate, samaccountname, displayname, mail, position, company, country, office, division, department, manager, mobile, disabled):
def ADUsers_Add(aduser):
    NowDate = str(datetime.now())[:-7]

    doc = {'changedate': NowDate,
           'samaccountname': aduser.SamAccountName,
           'displayname': aduser.DisplayName,
           'mail': aduser.Mail,
           'position': aduser.Position,
           'company': aduser.Company,
           'country': aduser.Country,
           'office': aduser.Office,
           'division': aduser.Division,
           'department': aduser.Department,
           'manager': aduser.Manager,
           'mobile': aduser.Mobile,
           'disabled': aduser.Disabled
    }
    coll_ad_users.save(doc)
