import os
import logging
basedir = os.path.abspath(os.path.dirname(__file__))

class Config:

    PROJECT_NAME = 'ALMBOARD'
    #PROJECT_CODE = 'ALMBOARD'

    ####################################
    ###
    ### FLASK
    ###
    ####################################
    #FLASK_HOST = '127.0.0.1'
    #FLASK_PORT = '80'
    #SERVER_NAME = FLASK_HOST+':'+FLASK_PORT

    # Other
    CSRF_ENABLED = True
    SECRET_KEY = 'you-will-never-guess'

    ####################################
    ###
    ### MONGO DB
    ###
    ####################################
    MONGODB_HOST = 'almboard'
    MONGODB_PORT = 27017
    MONGODB_DB = 'almboard'

    # Collections
    MONGODB_COL_P4_USERS = 'P4_users'
    MONGODB_COL_P4_PERM = 'P4_permissions'
    MONGODB_COL_AD_USERS = 'AD_users'
    MONGODB_COL_CC_USERS = 'CC_users'
    MONGODB_COL_MAIL_LOG = 'Mail_Log'
    MONGODB_COL_TFS_PROJECTS = 'TFS_projects'

    ####################################
    ###
    ### PERFORCE Cfg
    ###
    ####################################
    PERFORCE_SRV = 'pf.avp.ru:1666' 
    PERFORCE_HOST = 'pf.avp.ru'
    PERFORCE_PORT = '1666'
    PERFORCE_USER = 'TFS_Integr_Dev'
    PERFORCE_PSW = 'WelcomeWelcome'
    PERFORCE_CLIENT = 'SHAKIROV-O'
    PERFORCE_LICENSES = 540         # количество купленных лицензий Perforce
    PERFORCE_PORT = 1666
    PERFORCE_HOST = 'pf.avp.ru'

    ####################################
    ###
    ### MAIL
    ###
    ####################################
    MAIL_SERVER = 'mail.avp.ru'  # 'mailinternal.avp.ru'
    MAIL_USER = 'almsupport'
    MAIL_PSW = 'yd4Sp#1ecISsVUeSZT@]OM6Z'
    #MAIL_PORT = 25
    #MAIL_USE_TLS = False
    #MAIL_USE_SSL = True
    #MAIL_USERNAME = 'your-gmail-username'
    #MAIL_PASSWORD = 'your-gmail-password'
    # administrator list
    #ADMINS = ['your-gmail-username@gmail.com']

    ####################################
    ###
    ### LOGGING
    ###
    ####################################
    LOGGER_NAME = "%s_log" % PROJECT_NAME
    LOG_FILENAME = "/var/tmp/app.%s.log" % PROJECT_NAME
    LOG_LEVEL = logging.INFO
    LOG_FORMAT = "%(asctime)s %(levelname)s\t: %(message)s" # used by logging.Formatter

    ####################################
    ###
    ### AUTH (Active Directory authentification)
    ### http://flask-ldap3-login.readthedocs.io/en/latest/quick_start.html#basic-application
    ####################################

    # Hostname of your LDAP Server
    LDAP_HOST = 'KLDC8.avp.ru'

    # Base DN of your directory
    LDAP_BASE_DN = 'OU=Accounts,OU=Clients,DC=avp,DC=ru' #'DC=avp,DC=ru'

    # Users DN to be prepended to the Base DN
    #LDAP_USER_DN = 'ou=Accounts' #OU=Accounts,OU=Clients,DC=avp,DC=ru

    @staticmethod
    def init_app(app):
        pass

    @staticmethod
    def new():
        return Config

class TestingConfig(Config):
    DEBUG = True

    #FLASK_HOST = '127.0.0.1'
    #FLASK_PORT = '5030'
    #SERVER_NAME = FLASK_HOST+':'+FLASK_PORT

class ProdConfig(Config):
    DEBUG = False

    #FLASK_HOST = '127.0.0.1' #'0.0.0.0'
    #FLASK_PORT = '80'
    #SERVER_NAME = FLASK_HOST+':'+FLASK_PORT


config = {
    'test': TestingConfig,
    'prod': ProdConfig
}
